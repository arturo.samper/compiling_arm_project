#include<stdint.h>
#include "main.h"

#define SRMA_START      0x20000000U
#define SRAM_SIZE       (128 * 1024) //128KB
#define SRAM_END        ((SRMA_START) + (SRAM_SIZE))
#define STACK_START     SRAM_END

extern uint32_t _etext;
extern uint32_t _sdata;
extern uint32_t _edata;
extern uint32_t _ebss;
extern uint32_t _sbss;
int main(void);

void Reset_handler(void);

/* Here "Default_Handler is an alias function name for "NMI_Handler
   It means that, in the vector table array, address of "Default_Handler function will be stored
   So, when NMI exception triggers, "Default_Handler" will be executed,
   and We are going to do exactly the same with  te remaining exceptions
    
   Now, We also are making the functions weak. This is because, we are allowing programmer to override
   this function with same function name in main application. There programmer can implement real
   implementation of handling that exception.
*/
/* NVIC Interrupts */
void NMI_Handler(void)              __attribute__ ((weak, alias("Default_Handler")));
void HardFault_Handler(void)        __attribute__ ((weak, alias("Default_Handler")));
void MemManage_Handler(void)        __attribute__ ((weak, alias("Default_Handler")));
void BusFault_Handler (void)        __attribute__ ((weak, alias("Default_Handler")));
void UsageFault_Handler(void)       __attribute__ ((weak, alias("Default_Handler")));
void SVC_Handler(void)              __attribute__ ((weak, alias("Default_Handler")));
void DebugMon_Handler(void)         __attribute__ ((weak, alias("Default_Handler")));
void PendSV_Handler(void)           __attribute__ ((weak, alias("Default_Handler")));
void SysTick_Handler(void)          __attribute__ ((weak, alias("Default_Handler")));
void WWDG_IRQHandler(void)          __attribute__ ((weak, alias("Default_Handler")));
void PVD_IRQHandler(void)           __attribute__ ((weak, alias("Default_Handler")));             
void TAMP_STAMP_IRQHandler(void)    __attribute__ ((weak, alias("Default_Handler")));      
void RTC_WKUP_IRQHandler(void)      __attribute__ ((weak, alias("Default_Handler")));                               
void RCC_IRQHandler(void)           __attribute__ ((weak, alias("Default_Handler")));
/* Peripherals Interrupts*/
void Periperhal_0_IRQHandler(void)           __attribute__ ((weak, alias("Default_Handler")));
void Periperhal_1_IRQHandler(void)           __attribute__ ((weak, alias("Default_Handler")));

void Default_Handler(void);

//With the compiler directive __attribute__((section("name_of_new_memory_section"))) We are storing the created array vectors in a different memory place, and not in .data
uint32_t vectors[] __attribute__((section(".isr_vector"))) = {
    STACK_START,
    (uint32_t)&Reset_handler,
    (uint32_t)&NMI_Handler,
    (uint32_t)&HardFault_Handler,
    (uint32_t)&MemManage_Handler,
    (uint32_t)&BusFault_Handler,
    (uint32_t)&UsageFault_Handler,
    0,                              //Reserved the range 0x0000_001C - 0x0000_002B 
    0,
    0,
    0,
    (uint32_t)&SVC_Handler,
    (uint32_t)&DebugMon_Handler,
    0,
    (uint32_t)&PendSV_Handler,
    (uint32_t)&SysTick_Handler,
    (uint32_t)&WWDG_IRQHandler,
    (uint32_t)&PVD_IRQHandler,
    (uint32_t)&TAMP_STAMP_IRQHandler,
    (uint32_t)&RTC_WKUP_IRQHandler,
    (uint32_t)&RCC_IRQHandler,
    (uint32_t)&Periperhal_0_IRQHandler,
    (uint32_t)&Periperhal_1_IRQHandler,
};

/*
IMPORTANT: In 'C' program, if you just write '_edata', then it means
you want to access the value from a memory location (0x20000004) associated
with that variable. These is no "value" stored in any memory location for
this symbol "e_data".

So, you must use '&_edata'. this will give you the corresponding symbol value
(an address in this case) from symbol table. That is 0x20000004. 

PIENSA COMO SI ESTOS SYMBOLOS FUERAN VARIABLES. Cuando defines un puntero
se le asigna la dirección de memoria de la variable a la que va a apuntar.
Pues esto es lo mismo
*/
void Reset_handler(void){
    //Copy .data section to SRAM
    uint32_t size = (uint32_t*)&_edata - (uint32_t*)&_sdata; 
    uint8_t *ptrDest = (uint8_t*)&_sdata;
    uint8_t *ptrSrc = (uint8_t*)&_etext;
    for(uint32_t i=0; i<size; i++)
    {
        *ptrDest++ = *ptrSrc;
    }
    //Init the .bss section to zero in SRAM
    size = (uint32_t*)&_ebss - (uint32_t*)&_sbss; 
    ptrDest = (uint8_t*)&_sbss;
    for(uint32_t i=0; i<size; i++)
    {
        *ptrDest++ = 0x00;
    }
    // Call init function of standard(std) library (current step is only needed if you are using standard libraries in your code such as printf, scanf or string etc. Because otherwise standard functions will not work )
    // call main()
    main();
}

/*
Note : for the STM32F4 We have got 97 exceptions (15 system exceptions + 82 Interrputs(in our designs in Analog it number may be less, depending on the number of peripherals))
So, would you write handlers for all the exceptions? 
Not required. Let's create a single default handler for all the exceptions and allow programmer to implement required handlers as per application requirements.
*/
void Default_Handler(void)
{
    while(1);
}